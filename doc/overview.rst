##################
Overview
##################

On the first screen, you will see the dashboard.

.. image:: img/app_homescreen.jpg
    :alt: alternate text
    :height: 600px
    :align: center

.. note::
    Before you go on, make sure that you already done the `configuration`_ step

.. _configuration: configuration.html

They are two different main functionalities.

* :doc:`1. Graph views <../graphview>`
* :doc:`2: Dynamic reports<../dynamicreport>`
