##################
Dataflow OTRS
##################

In the standard version the following categories can be imported:

- General
- Service assignment
- Contact assignment
- Location assignment

Additional categories can also be synchronized. Please speak with our sales department. (info@becon.de)

The following synchronization methods are supported:

- Create
- Delete
- Update

Sync can be triggered automatically or manually. The relevant setting can be found in SysConfig.
