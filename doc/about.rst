##################
About us
##################

We love technology!!! 

.. image:: img/becon_logo.png
    :alt: We love technology
    :align: left

Since we went into business in 1988, `becon`_ has evolved from one of the first IT service providers to a developer of comprehensive solutions and services in the information and telecommunication technology industry. 

Our mission is to implement and optimize data center solutions. We focus on automating processes using both open and proprietary software. We are a full-service partner delivering the entire spectrum of value-added services from consulting, strategy, installation, training, rollout, development, and support to operational assistance for middle and large-sized organizations. 

`becon`_ is a private company with locations in Munich, Berlin and Fulda. Thanks to our Germany-wide coverage we are able to provide our customers with on-location service within just a few hours - for cases in which security concerns make remote support inadvisable or impossible. 
Privacy and data security are essential factors in achieving a reliable IT operation. The strict requirements in this area have led to a significant increase in awareness for security issues as well as to the development of sophisticated protection and emergency strategies. 



OTRSC is an OTRS package which is licensed under the `becon license`_.


.. _becon: https://www.becon.de/
.. _becon license: LICENSE.html
