##################
OTRS
##################

Sign on to the OTRS environment as Admin. Proceed to the tab:

Admin->Paket-Verwaltung

Upload the provided package here. ( I-doitConnector-XXX.opm ).

 .. note::
    Achten sie darauf, dass vorher das ITSMConfigurationManagement und ITSMServiceManagement Paket von OTRS installiert ist. Dieses Paket beinhaltet u.a die benötigte CMDB von OTRS.


The tab „SysConfig“ provides a number of settings with which you can, for example, define the synchronization interval.


.. image:: img/paketmanager.jpg
    :height: 200px
    :alt: home screen
    :align: center


.. image:: img/inst_first.jpg
    :height: 200px
    :alt: after installation 1 
    :align: center

The tab „SysConfig“ provides a number of settings with which you can, for example, define the synchronization interval.

Brief overview of the settings:

-	I-doitSyncInterval
-	DeleteCMDB
-	IncidentStateMap
-	IncidentStateDefault
-	DeploymentState2Functionality
-	DeploymentStateDefault 
-	ObjectTypeCategoryConfig
-	ContactAssignment::Max
-	ContactAssignment::Role
-	DefaultCompanyName

Now click on the connector to see additional options.

.. image:: img/inst_second.jpg
    :height: 300px
    :alt: after installation 2
    :align: center

Here you will find five tabs. Click on the first one 'Connector' to enter the i-doit connector. For this to work you will have to have entered the i-doit URL and the APIKey. If everything is green then the connector is set up correctly. 


.. image:: img/connector.jpg
    :height: 100px
    :alt: connector configuration
    :align: center


Then click on the tab 'Object Type Configuration' to define your CMDB structure, which you might want to synchronize by OTRS. (eg. only clients and servers)


.. image:: img/object_types.jpg
    :height: 100px
    :alt: object-type configuration
    :align: center

In the next step, choose the categories you want to synchronize.

.. image:: img/category.jpg
    :height: 100px
    :alt: category configuration
    :align: center

Now save the configuration.

The CMDB structure will now be installed in the background after which the first synchronization will take place. 


.. image:: img/sync.jpg
    :height: 100px
    :alt: sync
    :align: center