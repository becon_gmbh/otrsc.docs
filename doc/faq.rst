##################
FAQ
##################

**Synchronization won't start! What do I do?**

Check whether the LWP::UserAgent Library is Version 6.36. The library is managed within OTRS in the Kernel/cpan-lib.

**The connection test in i-doit hangs up at "Start test". How do I fix this problem?**

Please unblock/allow access to the file „sync.php“ in „.htaccess“. Use the entry under „jsonrpc.php“ for orientation. You will find this file in the root folder of the i-doit application.

