##################
System requirements
##################

*********
i-doit
*********

- at least i-doit version 1.9
- PHP Curl

*********
OTRS
*********

- at least OTRS Version 5
- ITSMConfigurationManagement
- ITSMServiceLevelManagement
- Net::SSL ab Version 0.12
- Schedule::Cron ab Version 1.01
- LWP::UserAgent ab Version 6.36