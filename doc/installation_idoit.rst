##################
i-doit
##################

Now go to the module management in i-doit. Upload the provided package idoit_otrsc.zip.

.. image:: img/idoit_addon.jpg
    :height: 200px
    :alt: AddOn manager
    :align: center

Next, sign on to i-doit. You will find a new entry (OTRSC) in the tab 'Extras'.

.. image:: img/idoit_menu.jpg
    :height: 200px
    :alt: otrsc menu
    :align: center

First you will need to enter the OTRS host. An OTRS user must also be provided for authentication. We recommend that an OTRS user is created who is only authorized access to CMDB queries. 

.. image:: img/idoit_configuration.jpg
    :height: 200px
    :alt: configuration menu
    :align: center

The name of the OTRS connector is „Tenant“.

.. image:: img/tenant.png
    :height: 200px
    :alt: otrs tenant
    :align: center


You can now see the assigned tickets via a Config item.

.. image:: img/idoit_toolbar.jpg
    :height: 200px
    :alt: toolbar menu
    :align: center


Additionally, the ticket may be viewed via the back link in OTRS.

.. image:: img/idoit_ticket.jpg
    :height: 200px
    :alt: ticket overview
    :align: center
