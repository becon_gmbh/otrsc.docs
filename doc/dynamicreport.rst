##################
2: Dynamic reports
##################

The dynamic report wizard, let you integrate an i-doit report in your app. The result will be displayed in a table view.


*********
Add a report
*********

Please click on the plus button.

.. image:: img/app_homescreen_dynamic_report.jpg
    :alt: alternate text
    :height: 400px
    :align: center


Now you see a popup in the middle screen. Enter a title and a report-id.

.. image:: img/app_addreport.jpg
    :alt: alternate text
    :height: 600px
    :align: center

You get the right report-id from your i-doit system.

.. image:: img/idoit_report_manager.png
    :alt: alternate text
    :align: center

Click now ok, to save the report. After clicking save, you will see a report in a table view.

.. image:: img/app_showreport.jpg
    :alt: alternate text
    :height: 600px
    :align: center

Click back, to see a new tile in your dashboard.

.. image:: img/app_tilereport.jpg
    :alt: alternate text
    :height: 600px
    :align: center


*********
Delete a report
*********

Just use the red cross sign in a tile, to delete a report.
