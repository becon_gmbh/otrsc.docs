##################
Welcome to OTRSC documentation 
##################

OTRSC (OTRS Connector) serves as an interface between i-doit and OTRS. I-doit is the leading system and synchronizes the config items according to OTRS. The OTRS CMDB is then filled.

------------

This documentation is organized into a couple of sections:

.. _about-docs:

.. toctree::
    :maxdepth: 2
    :caption: About us

    about

.. _introduction-docs:

.. toctree::
    :maxdepth: 2
    :caption: Introduction

    architecture
    requirements
    dataflow_otrs
    dataflow_idoit

.. _installation-docs:

.. toctree::
    :maxdepth: 2
    :caption: Installation

    installation_otrs
    installation_idoit

.. _faq-docs:

.. toctree::
    :maxdepth: 2
    :caption: FAQ

    faq


##################
License
##################

`becon`_ © 2013-2020 becon GmbH

.. _becon: LICENSE.html
