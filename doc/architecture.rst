##################
Architecture
##################

OTRSC (OTRS Connector) serves as an interface between i-doit and OTRS. i-doit acts as the 'master system', synchronizing the config items in accordance with OTRS and filling the OTRS CMDB.

From the perspective of i-doit, the ticket link is queried via a live interface and displayed within a config item. 

.. image:: img/architecture.png
    :height: 150px
    :alt: alternate text
    :align: center